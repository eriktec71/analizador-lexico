import com.sun.istack.internal.NotNull;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.regex.Pattern;

public class Analizador {

    private final  HashMap<String, String> simbolos = new HashMap<>();
    private final ArrayList<String[]> errores = new ArrayList<>();
    private final StringBuffer archivoToken = new StringBuffer();

    public void analizarCodigo(@NotNull ArrayList<String> codigo) {
        if (codigo == null) return;
        int numLinea = 1;
        for (String linea : codigo) {
            StringTokenizer st = new StringTokenizer(linea);
            while (st.hasMoreElements()) {
                String lexema = st.nextToken();
                boolean bandera = true;
                for (Token token : Token.values()) {
                    boolean matches = Pattern.matches(token.getEr(), lexema);
                    if (matches) {
                        crearSimbolo(lexema, token, !simbolos.containsKey(lexema));
                        bandera = false;
                    }
                }
                if (bandera) {
                    String[] error = new String[]{ "ERRLX" + errores.size(), lexema, numLinea + "", "Error Lexico"};
                    crearError(error, errorExist(lexema));
                }
            }
            numLinea += 1;
            archivoToken.append("\n");
        }
    }

    private void crearSimbolo(String lexema, Token token, boolean exist) {
        String token1;
        if (exist) {
            token1 = token.name() + token.getContador();
            simbolos.put(lexema, token1);
            archivoToken.append(token1).append(" ");
            return;
        }
        token1 = simbolos.get(lexema);
        archivoToken.append(token1).append(" ");
    }

    private int errorExist(String lexema) {
        for (int k = 0; k < errores.size(); k += 1){
            if (errores.get(k)[1].equals(lexema)) return k;
        }
        return -1;
    }

    private void crearError(String[] error, int index) {
        if (index == -1) {
            errores.add(error);
            archivoToken.append(error[0]).append(" ");
            return;
        }
        String[] error1 = errores.get(index);
        error1[2] += "," + error[2];
        errores.set(index, error1);
        archivoToken.append(error[0]).append(" ");
    }

    public ArrayList<String> lector() {
        ArrayList<String> codigo = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader("Programa.txt"));
            int caracter = 0;
            while ((caracter = br.read()) != -1) {
                String linea = br.readLine();
                linea = (char) caracter + (linea == null ? "" : linea);
                codigo.add(linea);
            }
            br.close();
        } catch (IOException e) { }
        return codigo;
    }

    public ArrayList<String> lector(String codigo) {
        ArrayList<String> codigo1 = new ArrayList<>();
        String[] codigo2 = codigo.split("\n");
        Collections.addAll(codigo1, codigo2);
        return codigo1;
    }

    public void imprimirTablaSimbolo() {
        System.out.println("Tabla de Simbolos");
        System.out.println("Lexema\t\tToken");
        if (!simbolos.isEmpty()) {
            String[] lexemas = simbolos.keySet().toArray(new String[0]);
            Arrays.stream(lexemas).forEach(key -> System.out.println(key + "\t\t" + simbolos.get(key)));
        }
    }

    public HashMap<String, String> getSimbolos() {
        return simbolos;
    }

    public ArrayList<String[]> getErrores() {
        return errores;
    }

    public void imprimirArchivoToken() {
        try {
            PrintWriter pr = new PrintWriter("Archivo Tokens.txt");
            pr.println(archivoToken);
            pr.close();
        } catch (IOException e) { }
    }

    public void imprimirErrorLexico() {
        System.out.println("Tabla de Errores");
        System.out.println("Toeken Error\t\t\tLexema\t\t\tLinea\t\t\tDescripcion");
        for (String[] error : errores) {
            System.out.println(error[0] + "\t\t\t" + error[1] + "\t\t\t" + error[2] + "\t\t\t" + error[3]);
        }
    }
}
