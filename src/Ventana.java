import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Ventana extends JFrame {

    private DefaultTableModel modelTablaSimbolos;
    private DefaultTableModel modelTablaErrores;

    public Ventana() {
        super("Compilador");
        this.setLayout(null);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        this.setSize(800, 620);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        init();
    }

    private void init() {
        JPanel panel = new JPanel(null);
        panel.setSize(800, 620);
        JLabel label = new JLabel("Codigo");
        label.setBounds(100, 0, 200, 10);
        panel.add(label);
        JTextArea codigo = new JTextArea();
        codigo.setBounds(0, 15, 400, 250);
        panel.add(codigo);
        JLabel label1 = new JLabel("Tabla Token");
        label1.setBounds(450, 0, 200, 10);
        panel.add(label1);
        modelTablaSimbolos = new DefaultTableModel(new Object[][]{{"",""}}, new String[]{"Lexema", "Token"});
        JTable tablaSimbolos = new JTable(modelTablaSimbolos);
        tablaSimbolos.setBounds(30,40,200,300);
        JScrollPane sp = new JScrollPane(tablaSimbolos);
        sp.setBounds(400, 15, 400, 250);
        panel.add(sp);
        JLabel label2 = new JLabel("Tabla de Error");
        label2.setBounds(350, 280, 200, 10);
        panel.add(label2);
        modelTablaErrores = new DefaultTableModel(new Object[][]{{"", "", "", ""}}, new String[]{"Token Error", "Lexema", "Linea", "Descripción"});
        JTable tablaError = new JTable(modelTablaErrores);
        tablaError.setBounds(30,40,200,300);
        modelTablaErrores = (DefaultTableModel) tablaError.getModel();
        JScrollPane sp1 = new JScrollPane(tablaError);
        sp1.setBounds(0, 295, 800, 250);
        panel.add(sp1);
        JButton analizar = new JButton("Analizar");
        analizar.setBounds(10, 560, 100, 25);
        analizar.addActionListener(e -> {
            reiniciar();
            analizar(codigo.getText());
        });
        panel.add(analizar);
        this.add(panel);
    }

    private void reiniciar() {
        int rows = modelTablaSimbolos.getRowCount();
        for (int k = rows - 1; k >= 0; k -= 1){
            modelTablaSimbolos.removeRow(k);
        }
        rows = modelTablaErrores.getRowCount();
        for (int k = rows - 1; k >= 0; k -= 1){
            modelTablaErrores.removeRow(k);
        }
    }

    private void analizar(String codigo) {
        Analizador analizador = new Analizador();
        ArrayList<String> codigo1 = analizador.lector(codigo);
        analizador.analizarCodigo(codigo1);
        HashMap<String, String> tablaSimbolos = analizador.getSimbolos();
        String[] lexemas = tablaSimbolos.keySet().toArray(new String[0]);
        Arrays.stream(lexemas).forEach(key -> modelTablaSimbolos.addRow(new Object[]{key, tablaSimbolos.get(key)}));
        ArrayList<String[]> tablaError = analizador.getErrores();
        tablaError.forEach(row -> modelTablaErrores.addRow(row));
        analizador.imprimirArchivoToken();
    }

    public static void main(String[] args) {
        Ventana ventana = new Ventana();
        ventana.setVisible(true);
    }
}
