enum Token {
    WHILE("(while)") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    },
    TD("((\\$FLOTANTE\\$)|(\\$ENTERO\\$)|(\\$CARACTER\\$))") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    },
    IDE("(KP[0-9]{4}.)") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    },
    NUME("(\\-?[0-9]+66)") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    },
    NUMPD("(\\-?[0-9]+66\\.[0-9]+)") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    },
    OPELB("((&&)|(\\|\\|))") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    },
    OPER("(<|<=|>|>=|==|!=)") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    },
    SE("([(]|[)]|[{]|[}]|[,]|[;])") {
        private int cont = 0;

        @Override
        public int getContador() {
            cont += 1;
            return cont;
        }
    };

    private final String er;

    Token(String er) {
        this.er = er;
    }

    abstract int getContador();

    public String getEr() {
        return er;
    }
}
